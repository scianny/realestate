package com.real.estate.entity.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.real.estate.entity.Houses;

@RepositoryRestResource(collectionResourceRel="houses",path="houses")
public interface HousesRepository extends PagingAndSortingRepository<Houses, Integer> {

	Iterable<Houses> findAllById(Iterable<Integer> ids);
	
	Optional<Houses> findById(Integer id);
	List<Houses> findByAddress(String address);
	List<Houses> findByCity(@Param("city")String city);
	
}
