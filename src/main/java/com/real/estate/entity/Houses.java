package com.real.estate.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
public class Houses {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String address;
    private String city;

    public Houses() {
		// TODO Auto-generated constructor stub
	}
    
    public Houses(Integer id, String addess, String city) {
        this.id = id;
        this.address = addess;
        this.city = city;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address= address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}