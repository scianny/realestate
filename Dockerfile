FROM maven:3.6.3-openjdk-8
# COPYING SRC FILES
COPY src/ /home/build/src
COPY pom.xml /home/build/pom.xml
WORKDIR /home/build
# BUILD
RUN mvn clean install -Dmaven.test.skip=true


FROM openjdk:8-jdk-alpine
COPY --from=0 /home/build/target/realestate-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
# RUN JAVA
ENTRYPOINT ["java", "-jar", "/app.jar"]